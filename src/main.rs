use ggez::{Context, ContextBuilder, GameError, GameResult};
use ggez::event::{self, EventHandler};
use ggez::graphics::{self, Color, DrawParam, Image};
use ggez::input::{keyboard, mouse};
use ggez::timer;

fn main() -> GameResult {
    let ctx_builder = ContextBuilder::new(
        env!("CARGO_PKG_NAME"),
        "The Committee",
    );
    let ctx_builder = if cfg!(feature = "bundled") {
        ctx_builder.add_zipfile_bytes(include_bytes!(
            concat!(env!("OUT_DIR"), "/resources.zip")
        ).as_ref())
    } else {
        ctx_builder.add_resource_path(
            concat!(env!("CARGO_MANIFEST_DIR"), "/resources")
        )
    };
    let (mut ctx, event_loop) = ctx_builder.build()?;
    
    graphics::set_drawable_size(&mut ctx, 800.0, 600.0)?;
    graphics::set_resizable(&mut ctx, false)?;

    let game_state = GameState {
        x: 400,
        y: 300,
        dx: 0,
        dy: 0,
        player_image: Image::new(&mut ctx, "/player.png")?,
    };

    event::run(ctx, event_loop, game_state)
}

struct GameState {
    x: u16,
    y: u16,
    dx: i16,
    dy: i16,
    player_image: Image,
}

impl EventHandler<GameError> for GameState {
    fn update(&mut self, ctx: &mut Context) -> GameResult {
        if !timer::check_update_time(ctx, 60) {
            timer::yield_now();
            return Ok(());
        }

        if keyboard::is_key_pressed(ctx, keyboard::KeyCode::Left) {
            self.dx -= 1;
        }
        if keyboard::is_key_pressed(ctx, keyboard::KeyCode::Up) {
            self.dy -= 1;
        }
        if keyboard::is_key_pressed(ctx, keyboard::KeyCode::Right) {
            self.dx += 1;
        }
        if keyboard::is_key_pressed(ctx, keyboard::KeyCode::Down) {
            self.dy += 1;
        }

        if mouse::button_pressed(ctx, mouse::MouseButton::Left) {
            let mouse_pos = mouse::position(ctx);
            self.x = mouse_pos.x as u16;
            self.y = mouse_pos.y as u16;
            self.dx /= 2;
            self.dy /= 2;
        } else {
            self.x = (
                (self.x as i32 + self.dx as i32)
                    .rem_euclid(800)
            ) as u16;
            self.y = (
                (self.y as i32 + self.dy as i32)
                    .rem_euclid(600)
            ) as u16;
        }

        Ok(())
    }

    fn draw(&mut self, ctx: &mut Context) -> GameResult {
        graphics::clear(ctx, Color::WHITE);

        let extrap = timer::remaining_update_time(ctx).as_secs_f32()
            / 60.0;  // fps

        let x = self.x as f32 + extrap * self.dx as f32;
        let y = self.y as f32 + extrap * self.dy as f32;
        for x_wrap in [-1., 0., 1.] {
            for y_wrap in [-1., 0., 1.] {
                graphics::draw(
                    ctx,
                    &self.player_image,
                    DrawParam::new()
                        .offset([0.5, 0.5])
                        .dest([
                            x - 800.0 * x_wrap,
                            y - 600.0 * y_wrap,
                        ])
                )?;
            }
        }
        graphics::present(ctx)
    }
}
