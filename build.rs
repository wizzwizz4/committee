use std::env;
use std::fs::File;
use std::path::PathBuf;
use zip::{CompressionMethod, write::{FileOptions, ZipWriter}};
use zip_extensions::write::ZipWriterExtensions as _;

fn main() {
    let manifest_dir = PathBuf::from(env::var_os("CARGO_MANIFEST_DIR").unwrap());
    let out_dir = PathBuf::from(env::var_os("OUT_DIR").unwrap());

    let resources_zip = File::create(out_dir.join("resources.zip")).unwrap();
    ZipWriter::new(resources_zip)
        .create_from_directory_with_options(
            &manifest_dir.join("resources"),
            FileOptions::default()
                .compression_method(CompressionMethod::Bzip2),
        )
        .unwrap();
}
